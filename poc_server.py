#!/usr/local/bin/python2.7
import cherrypy
import ssl
import json
import os
import random
import string
import base64
import time
import subprocess
from datetime import datetime



class about:
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self, name=None):
        http_method = getattr(self,cherrypy.request.method)
        return (http_method)(name)
 
    def GET(self, name = None):
        useragent = cherrypy.request.headers.get('User-Agent')
        xff = cherrypy.request.headers.get('X-Forward-For')
        origin = cherrypy.request.headers.get('Origin')
        remote = cherrypy.request.headers.get('Remote-Addr')
        
        aboutapi = {'errorCode':200, 'apiVersion': '0beta1', 'user-agent': useragent, 'xff':xff, 'origin':origin, 'remote-addr':remote}
        return aboutapi
 

try:
    print("Loading configuration ...")
    with open("config.json") as config_json:
        config = json.load(config_json)

except Exception:
    message = "There was an error loading config !!"
    print(message)
        
cherrypy.server.socket_port=config['app_port']
cherrypy.server.socket_host='192.168.1.30'

root=about()
cherrypy.quickstart(root)   
